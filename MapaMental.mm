<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Fundamentos Te&#xf3;ricos da Computa&#xe7;&#xe3;o" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_67829734" CREATED="1521924220720" MODIFIED="1521929798390"><hook NAME="MapStyle" zoom="1.5">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Prova de Teoremas" FOLDED="true" POSITION="right" ID="ID_1285629132" CREATED="1521924495235" MODIFIED="1521924500714">
<node TEXT="Prova para a bicondicional" ID="ID_634424265" CREATED="1521927619000" MODIFIED="1521951073139">
<node TEXT="\latex prova-se \alpha \leftrightarrow \beta ao provar que \alpha \rightarrow \beta e \beta \rightarrow \alpha" ID="ID_1558972090" CREATED="1521951075777" MODIFIED="1521951169729"/>
</node>
<node TEXT="Prova para a Universal" ID="ID_1929549513" CREATED="1521927629433" MODIFIED="1521927634925">
<node TEXT="\latex prova-se \forall $x$ $P(x)$ provando $P(a)$ para um $a$ arbitr&#xe1;rio ausente das premissas" ID="ID_1187189535" CREATED="1521951184344" MODIFIED="1521951405489"/>
<node TEXT="\latex prova-se \forall $x$ \in $AP(x)$ provando $P(a)$ para um $a$ \in $A$ arbitr&#xe1;rio ausente das premissas" ID="ID_933347947" CREATED="1521951583008" MODIFIED="1521951674450"/>
</node>
<node TEXT="Prova por constru&#xe7;&#xe3;o" ID="ID_677971790" CREATED="1521927635686" MODIFIED="1521927643310">
<node TEXT="\latex prova-se que \exists $x P(x)$ provando $P(a)$ para um $a$ espec&#xed;fico" ID="ID_114352469" CREATED="1521951449067" MODIFIED="1521951571391"/>
</node>
<node TEXT="Prova por contradi&#xe7;&#xe3;o" ID="ID_1586936795" CREATED="1521927643893" MODIFIED="1521927646968">
<node TEXT="\latex prova-se \alpha provando que \neg \alpha &#xe9; uma contradi&#xe7;&#xe3;o" ID="ID_722940203" CREATED="1521951687050" MODIFIED="1521951736233"/>
</node>
<node TEXT="Prova por an&#xe1;lise de casos" ID="ID_1027293369" CREATED="1521927647333" MODIFIED="1521927655458">
<node TEXT="\latex prova-se $\beta$ provando $\alpha_{1} \lor \alpha_{2} \lor ... \lor \alpha_{n}$ e, em seguida:&#xa;\\&#xa;&#xa;1)supondo $\alpha_{1}$ prova $\beta$&#xa;\\&#xa;&#xa;2)supondo $\alpha_{2}$ prova $\beta$&#xa;\\&#xa;  .&#xa;\\&#xa;  .&#xa;\\&#xa;  .&#xa;\\&#xa;&#xa;N)supondo $\alpha_{n}$ prova $\beta$" ID="ID_1692334679" CREATED="1521951743602" MODIFIED="1521952333511"/>
</node>
<node TEXT="Regra de infer&#xea;ncia" ID="ID_1083600831" CREATED="1521927661304" MODIFIED="1521929313774">
<node TEXT="https://pt.wikipedia.org/wiki/Lista_de_regras_de_infer&#xea;ncia#Tabela:_Regras_de_Infer&#xea;nciaRe" ID="ID_1167718682" CREATED="1521952367814" MODIFIED="1521952433657" LINK="https://pt.wikipedia.org/wiki/Lista_de_regras_de_infer&#xea;ncia#Tabela:_Regras_de_Infer&#xea;nciaRe"/>
</node>
</node>
<node TEXT="Linguagens formais" POSITION="left" ID="ID_1048195828" CREATED="1521924519704" MODIFIED="1521924526624"/>
<node TEXT="Gram&#xe1;ticas" POSITION="left" ID="ID_622035768" CREATED="1521924529102" MODIFIED="1521924531969"/>
<node TEXT="Problemas de decis&#xe3;o" POSITION="left" ID="ID_1103816985" CREATED="1521924532276" MODIFIED="1521924537888"/>
<node TEXT="Rela&#xe7;&#xf5;es" FOLDED="true" POSITION="right" ID="ID_1581189594" CREATED="1521924502015" MODIFIED="1521924506231">
<node TEXT="Dom&#xed;nio, Contra-dom&#xed;nio e Imagem" ID="ID_788175524" CREATED="1521958453496" MODIFIED="1521958865843">
<hook URI="injetora.jpg" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Dom&#xed;nio: A" ID="ID_1203508287" CREATED="1521958868431" MODIFIED="1521958879222"/>
<node TEXT="Contra-dom&#xed;nio: B" ID="ID_746664328" CREATED="1521958879895" MODIFIED="1521958887368"/>
<node TEXT="Imagem: {1,3,5}" ID="ID_1471315195" CREATED="1521958888560" MODIFIED="1521958896169"/>
</node>
<node TEXT="" ID="ID_696694717" CREATED="1521959006370" MODIFIED="1521959006370">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="Nota&#xe7;&#xe3;o" ID="ID_1836372581" CREATED="1521958482369" MODIFIED="1521958487709">
<node TEXT="\latex (x,y) \in R" ID="ID_1515885061" CREATED="1521958907353" MODIFIED="1521958978632"/>
<node TEXT="xRy" ID="ID_1388551348" CREATED="1521958979492" MODIFIED="1521958989485"/>
</node>
<node TEXT="" ID="ID_1649433498" CREATED="1521959006368" MODIFIED="1521959006370">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Qualquer uma serve" ID="ID_645440340" CREATED="1521959006371" MODIFIED="1521959010670"/>
</node>
<node TEXT="Propriedades de rela&#xe7;&#xf5;es" ID="ID_1884044947" CREATED="1521958489080" MODIFIED="1521958500967">
<node TEXT="Reflexiva" ID="ID_565685577" CREATED="1521959017134" MODIFIED="1521959020928">
<node TEXT="\latex xRx \forall x \in A" ID="ID_1678176212" CREATED="1521959029567" MODIFIED="1521959046816"/>
</node>
<node TEXT="Sim&#xe9;trica" ID="ID_81474546" CREATED="1521959021356" MODIFIED="1521959024271">
<node TEXT="\latex xRy \rightarrow yRx \forall x,y \in A" ID="ID_1381956069" CREATED="1521959052365" MODIFIED="1521959084274"/>
</node>
<node TEXT="Transitiva" ID="ID_1912358262" CREATED="1521959024665" MODIFIED="1521959027903">
<node TEXT="\latex (xRy,yRz) \rightarrow xRz \forall x,y,z \in A" ID="ID_328309223" CREATED="1521959087463" MODIFIED="1521959125747"/>
</node>
</node>
<node TEXT="Equival&#xea;ncia" ID="ID_14406816" CREATED="1521958502266" MODIFIED="1521958517299">
<node TEXT="Rela&#xe7;&#xe3;o bin&#xe1;ria reflexiva, sim&#xe9;trica e transitiva" ID="ID_1313465531" CREATED="1521959147672" MODIFIED="1521959165826"/>
<node TEXT="Classes de equival&#xea;ncia" ID="ID_1018627415" CREATED="1521959166214" MODIFIED="1521959173341">
<node TEXT="Parti&#xe7;&#xe3;o do conjunto sobre o qual h&#xe1; uma rela&#xe7;&#xe3;o de equival&#xea;ncia" ID="ID_1352558359" CREATED="1521959179405" MODIFIED="1521959205237"/>
</node>
</node>
<node TEXT="Fecho" ID="ID_1715103030" CREATED="1521958517733" MODIFIED="1521958520508">
<node TEXT="\latex Menor rela&#xe7;&#xe3;o S \mid S \supseteq R que possui uma propriedade espec&#xed;fica" ID="ID_1951982132" CREATED="1521959212166" MODIFIED="1521959402947">
<node TEXT="Fecho Reflexivo" ID="ID_710640693" CREATED="1521959427415" MODIFIED="1521959434683"/>
<node TEXT="Fecho Sim&#xe9;trico" ID="ID_1543713360" CREATED="1521959435099" MODIFIED="1521959438190"/>
<node TEXT="Fecho Transitivo" ID="ID_622860851" CREATED="1521959438693" MODIFIED="1521959441363"/>
</node>
</node>
</node>
<node TEXT="Conjuntos Enumer&#xe1;veis" FOLDED="true" POSITION="right" ID="ID_664760876" CREATED="1521924508781" MODIFIED="1521924512690">
<node TEXT="&quot;Tamanho&quot; do Conjunto" ID="ID_925015231" CREATED="1522003010531" MODIFIED="1522003042622">
<node TEXT="Conjuntos Finitos" ID="ID_522637461" CREATED="1522003018029" MODIFIED="1522003022828">
<node TEXT="N&#xfa;mero de elementos" ID="ID_1711691710" CREATED="1522003049187" MODIFIED="1522003054818"/>
</node>
<node TEXT="Conjuntos Infinitos" ID="ID_471675558" CREATED="1522003023305" MODIFIED="1522003027270">
<node TEXT="Cardinalidade" ID="ID_207405308" CREATED="1522003056916" MODIFIED="1522003061391">
<node TEXT="Dois Conjuntos possui mesma cardinalidade se h&#xe1; uma fun&#xe7;&#xe3;o bijetora entre eles" ID="ID_1918175466" CREATED="1522003063036" MODIFIED="1522003088176"/>
</node>
</node>
</node>
<node TEXT="Conjuntos Enumer&#xe1;veis" ID="ID_1843986874" CREATED="1522002984444" MODIFIED="1522002989286">
<node TEXT="Conjuntos de mesma cardinalidade que N" ID="ID_879922443" CREATED="1522003093176" MODIFIED="1522003233522"/>
</node>
<node TEXT="Conjuntos Cont&#xe1;veis" ID="ID_1963913987" CREATED="1522002976932" MODIFIED="1522002984050">
<node TEXT="Conjuntos finitos ou enumer&#xe1;veis" ID="ID_114819964" CREATED="1522003242541" MODIFIED="1522003250511"/>
<node TEXT="&#xc9; condi&#xe7;&#xe3;o suficiente n&#xe3;o ter n&#xfa;meros irrepresent&#xe1;veis por um n&#xfa;mero finito de s&#xed;mbolos" ID="ID_1227703311" CREATED="1522003253038" MODIFIED="1522003306080">
<node TEXT="Como no conjunto dos n&#xfa;meros irracionais" ID="ID_1240736506" CREATED="1522003307640" MODIFIED="1522003319397"/>
</node>
</node>
</node>
<node TEXT="Defini&#xe7;&#xf5;es recursivas" FOLDED="true" POSITION="right" ID="ID_902644688" CREATED="1521924513143" MODIFIED="1521924519336">
<node TEXT="Defini&#xe7;&#xe3;o recursiva de conjuntos enumer&#xe1;veis" ID="ID_31136749" CREATED="1522030141435" MODIFIED="1522030152834">
<node TEXT="Especifica gera&#xe7;&#xe3;o de conjunto cont&#xe1;vel" ID="ID_1497317673" CREATED="1522030174247" MODIFIED="1522030194797">
<node TEXT="Por meio da aplica&#xe7;&#xe3;o de finitas opera&#xe7;&#xf5;es em seu subconjunto" ID="ID_1348877820" CREATED="1522030196184" MODIFIED="1522030225369"/>
</node>
</node>
<node TEXT="Passos da defini&#xe7;&#xe3;o recursiva" ID="ID_663924960" CREATED="1522029995630" MODIFIED="1522030028728">
<node TEXT="Base" ID="ID_999528073" CREATED="1522030030435" MODIFIED="1522030033225">
<node TEXT="\latex Especifica B \subset A" ID="ID_1573368459" CREATED="1522030247458" MODIFIED="1522030266128"/>
</node>
<node TEXT="Passo recursivo" ID="ID_56852800" CREATED="1522030033805" MODIFIED="1522030038033">
<node TEXT="Especifica opera&#xe7;&#xf5;es, que, aplicadas em elementos de A, geram elementos de A" ID="ID_1238060471" CREATED="1522030279980" MODIFIED="1522030305586"/>
</node>
<node TEXT="Fechamento" ID="ID_506518843" CREATED="1522030038665" MODIFIED="1522030137987">
<node TEXT="Obten&#xe7;&#xe3;o de A aplicando opera&#xe7;&#xf5;es do passo recursivo em B" ID="ID_1912559751" CREATED="1522030309913" MODIFIED="1522030326636"/>
</node>
</node>
</node>
<node TEXT="Indu&#xe7;&#xe3;o Matem&#xe1;tica" FOLDED="true" POSITION="right" ID="ID_1628238217" CREATED="1522030050946" MODIFIED="1522030065250">
<node TEXT="Passos da indu&#xe7;&#xe3;o matem&#xe1;tica" ID="ID_839329684" CREATED="1522030066875" MODIFIED="1522030074966">
<node TEXT="Base da indu&#xe7;&#xe3;o" ID="ID_1557088670" CREATED="1522030081699" MODIFIED="1522030088706">
<node TEXT="provar que P se verifica para o menor valor de n" ID="ID_822002002" CREATED="1522030336774" MODIFIED="1522030358409">
<node TEXT="\latex Se n \in N e n&#xe3;o possui limita&#xe7;&#xf5;es $min(n_i) = 0$" ID="ID_628344521" CREATED="1522030385883" MODIFIED="1522030427115"/>
<node TEXT="\latex Se n \in N, k \leq n \rightarrow $min(n_i) = k$" ID="ID_576049405" CREATED="1522030430172" MODIFIED="1522030488457"/>
</node>
</node>
<node TEXT="Hip&#xf3;tese de indu&#xe7;&#xe3;o" ID="ID_1735262916" CREATED="1522030089033" MODIFIED="1522030098709">
<node TEXT="Supor que P se verifica para um n arbitr&#xe1;rio" ID="ID_1857088619" CREATED="1522030365204" MODIFIED="1522030383683"/>
</node>
<node TEXT="Passo indutivo" ID="ID_1007652097" CREATED="1522030099261" MODIFIED="1522030101864">
<node TEXT="Provar que P se verifica para n+1" ID="ID_1273643694" CREATED="1522030497080" MODIFIED="1522030513981"/>
</node>
</node>
<node TEXT="Indu&#xe7;&#xe3;o forte" ID="ID_1743446402" CREATED="1522030075219" MODIFIED="1522030078600">
<node TEXT="Hip&#xf3;tese de indu&#xe7;&#xe3;o" ID="ID_802756613" CREATED="1522030104561" MODIFIED="1522030110881">
<node TEXT="\latex Supor que P se verifica \forall k &lt; n" ID="ID_6912860" CREATED="1522030525973" MODIFIED="1522030564765"/>
</node>
<node TEXT="Passo indutivo" ID="ID_704334717" CREATED="1522030111124" MODIFIED="1522030114062">
<node TEXT="Provar que P se verifica para n" ID="ID_556962536" CREATED="1522030571512" MODIFIED="1522030586635"/>
</node>
</node>
</node>
</node>
</map>
